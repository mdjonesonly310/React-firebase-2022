import React from 'react'
import PropTypes from "prop-types";

const FeedbackList = ({feedback}) => {
    if(!feedback && feedback.length === 0){
        return <p>No Feedback Yet</p>
    }
  return (
    <div>List</div>
  )
}


FeedbackList.propTypes = {
    feedback: PropTypes.arrayOf(

        PropTypes.shape({
            id: PropTypes.number.isRequired,
            text:PropTypes.string.isRequired,
            rating: PropTypes.number.isRequired
        })
    )
}

export default FeedbackList